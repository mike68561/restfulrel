import time
import flask
import json

class InternalMethods():
    def __init__(self):
        """
        Instantiate
        """
        pass

    def db_connect(self, db, user, pass_word, mode):
        (db_version, db_value, db_request) = db
        (user_type, user_value, user_request) = user
        (pass_type, pass_value, pass_request) = pass_word
        (mode_type, mode_value, mode_request) = mode
        conect_address = "connectTo '" + db_value.strip() + "' '" + user_value.strip() + "' '" + pass_value.strip() + "' '" + mode_value + "'"

        user_conn = eval(conect_address)
        return user_conn

    def rest_call(self, connection, action, path):
        """
        This function provides REST call functionality and provides JSON object for curl/browser requests.
        """
        approved_actions = {'native': True}
        action = action.lower()

        if action not in approved_actions:
            return 'Invalid DB operation'

        params = dict()
        for (k, v) in flask.request.args.iteritems():
            params[k.lower()] = v
        results = SQL on connection """ """params['query']
        json_results = json.dumps(results)

        return json_results

