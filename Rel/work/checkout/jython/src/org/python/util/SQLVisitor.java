package org.python.util;




import net.sf.jsqlparser.*;
import net.sf.jsqlparser.expression.*;
import net.sf.jsqlparser.expression.operators.arithmetic.*;
import net.sf.jsqlparser.expression.operators.conditional.*;
import net.sf.jsqlparser.expression.operators.relational.*;
import net.sf.jsqlparser.parser.*;
import java.io.StringReader; //hopefully this works
import net.sf.jsqlparser.schema.*;
import net.sf.jsqlparser.statement.*;
import net.sf.jsqlparser.statement.create.table.*;
import net.sf.jsqlparser.statement.delete.*;
import net.sf.jsqlparser.statement.drop.*;
import net.sf.jsqlparser.statement.insert.*;
import net.sf.jsqlparser.statement.replace.*;
import net.sf.jsqlparser.statement.select.*;
import net.sf.jsqlparser.statement.truncate.*;
import net.sf.jsqlparser.statement.update.*;
import net.sf.jsqlparser.util.deparser.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.DatabaseMetaData;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleResultSet;
import oracle.jdbc.pool.OracleDataSource;
import org.python.core.SPARQLDoer;
import org.python.util.SQLValidator;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.lang.String;
import java.lang.Character;
import java.text.DecimalFormatSymbols;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Stack;

import org.python.core.PyObject;
import org.python.core.PyRelConnection;
import org.python.core.SPARQLDoer;

public class SQLVisitor implements SelectVisitor, FromItemVisitor, ExpressionVisitor, ItemsListVisitor, SelectItemVisitor, OrderByVisitor {
	private List<String> filters;
	private List<String> matches;
	private HashMap<String, String> tablesAliases;
	private LinkedHashMap<String, String> columnsAs;
	private List<String> internalColumns;
	private HashMap<String, List<String>> tablesColumns;
	private static String temp;
	private String ownException;
	private Boolean wasEquals;
	private Boolean subselect;
	private int subDepth;
	private static int joinInc = 1; // for each join this will be incremented and used in a variable name.
	public String site;
	
	private String tablenameFrom = "";
	public String url = "";
	public String uname = "";
	public String pword = "";
	public OracleDataSource ods;
	public PyRelConnection connection;
	public Statement stmt;

	public CCJSqlParserManager parserManager = new CCJSqlParserManager();
	
	static private HashMap<String, String> map  = new HashMap<String, String>();
	
	/**
	 *
	 */
	public SQLVisitor(PyObject conn) {
		this.connection = (PyRelConnection)conn;
	}
	
	/**
	 *
	 */
	public void doDrop(Drop stmt, OracleConnection conn) {
		String tableToDrop = stmt.getName();
		String website = "www.example.org/";
		String command = "";
/*
		if ( tableToDrop.equals("RDF_DATA") ) {
			System.out.println("Dropping the RDF model and everything associated with it: " + tableToDrop);
			//Do the RDF data drop of models and everything else
			try {
                conn.createStatement().executeStatement("DROP SEQUENCE RDF_DATA_TABLE_SQNC");
			} catch (SQLException e) {
				System.out.println("Failed to drop RDF data table sequence\n" + e);
			}
                        try {
				conn.createStatement().executeStatement("DROP SEQUENCE RDF_GUID_SQNC");       
			} catch (SQLException e) {
				System.out.println("Failed to drop RDF GUID sequence\n" + e);
			}
			try {
                        	conn.createStatement().executeStatement("BEGIN\nSEM_APIS.DROP_RDF_MODEL('" + connection.getModel() + "');\nEND;");
			} catch (SQLException e) {
				System.out.println("Failed to drop RDF model: RDF_MODEL_" +uname.toUpperCase() + "\n" + e);
			}
			try {
                        	conn.createStatement().executeStatement("DROP TABLE RDF_DATA_TABLE");
			} catch (SQLException e) {
				System.out.println("Failed to drop RDF data table\n" + e);
			}

		} else {

			System.out.println("Dropping just one named graph from the RDF data: " + tableToDrop);
			command = "DELETE from RDF_DATA_TABLE a where a.triple.GET_MODEL() = '" + connection.getModel()  + ":<" + website + tableToDrop + ">'";
			try {
				conn.createStatement().executeStatement(command);
				System.out.println("|" + command + "|");
			} catch (SQLException e) {
				System.out.println("Failed to drop RDF named graph: " + tableToDrop);
				System.out.println(e);
			}
		}
*/
	}
	
	/**
	 *
	 */

    public void doInsert(Insert stmt) throws SQLException {
        if (stmt.getColumns() != null) {

            Iterator valsIt = ((ExpressionList)stmt.getItemsList()).getExpressions().iterator();
            String id = Integer.toString(SPARQLDoer.getNextGUID(connection));
            String subject = id; 
            for (Iterator colsIt = stmt.getColumns().iterator(); colsIt.hasNext(); ) {
                String attr = ((Column)colsIt.next()).getColumnName().replaceAll("'", "").replaceAll("\"", "");
                Object attrValue = valsIt.next(); 
                String valStr = (attrValue.toString().replaceAll("'", "")).replaceAll("\'", "").replaceAll("\"", "");
                // Determine a type string for our object being inserted.
                String typeString = null;
                if (attrValue instanceof LongValue)
                {
                    typeString = "integer";
                }
                else if(attrValue instanceof DoubleValue)
                {
                        typeString = "float";
                }
                else
                {
                    if(valStr.toUpperCase().equals("TRUE") || valStr.toUpperCase().equals("FALSE"))
                    {
                        typeString = "boolean";
                    }
                    else
                    {
                        typeString = "string";
                    }
                }
                subject = SPARQLDoer.insertDataPropQuad(connection, subject, attr, valStr, stmt.getTable().toString(), typeString);
                if(map.get(stmt.getTable().toString() + "." + attr) == null) {
                    SPARQLDoer.insertObjectPropQuad(connection, attr, "rdf:type", "owl:DatatypeProperty");
                    SPARQLDoer.insertObjectPropQuad(connection, attr, "rdfs:domain", stmt.getTable().toString()); 
                    SPARQLDoer.insertObjectPropQuad(connection, attr, "rdf:range", "rdfs:xsd:" + typeString);
                    SPARQLDoer.insertObjectPropQuad(connection, attr, "rdf:type", "owl:FunctionalProperty");
                }
                map.put(stmt.getTable().toString() + "." + attr, "true" );
            }
            // Add the DBUNIQUEID
            SPARQLDoer.insertDataPropQuad(connection, subject, "DBUNIQUEID", id, stmt.getTable().toString(), "integer");
            if(map.get(stmt.getTable().toString() + ".DBUNIQUEID") == null) {
                SPARQLDoer.insertObjectPropQuad(connection, "DBUNIQUEID", "rdf:type", "owl:DatatypeProperty");
                SPARQLDoer.insertObjectPropQuad(connection, "DBUNIQUEID", "rdfs:domain", stmt.getTable().toString()); 
                SPARQLDoer.insertObjectPropQuad(connection, "DBUNIQUEID", "rdf:range", "rdfs:xsd:integer");
                SPARQLDoer.insertObjectPropQuad(connection, "DBUNIQUEID", "rdf:type", "owl:FunctionalProperty");
            }
            map.put(stmt.getTable().toString() + ".DBUNIQUEID", "true" );

            SPARQLDoer.insertDVAMetaData(connection, subject, stmt.getTable().toString());
        }
    }
	
	/**
	 *
	 */
    public void doCreateTable(CreateTable stmt) throws SQLException {
        String modelName = connection.getModel();
        String tableName = stmt.getTable().getName();
        //String namedGraph = "<www.example.org/" + tableName + ">";
        String namedGraph = "";
        String attribute;
        String type;
        String xsdType;

        SPARQLDoer.createQuadStore(connection);
        SPARQLDoer.insertObjectPropQuad(connection, namedGraph, "rdf:type", "rdfs:Class");

        if (stmt.getColumnDefinitions() != null) {
            for (Iterator colsIt = stmt.getColumnDefinitions().iterator(); colsIt.hasNext(); ) {
                ColumnDefinition col = (ColumnDefinition)colsIt.next();
                attribute = col.getColumnName();
                type = col.getColDataType().getDataType();

                if (type.toLowerCase().equals("numeric") || type.toLowerCase().equals("decimal") ||
                    type.toLowerCase().equals("real")) {
                    xsdType = "xsd:decimal"; // was decimal
                } else if (type.toLowerCase().equals("varchar") || type.toLowerCase().equals("varchar2")) {
                    xsdType = "xsd:string"; // was string
                } else if (type.toLowerCase().equals("bit") || type.toLowerCase().equals("tinyint") ||
                           type.toLowerCase().equals("bigint")) {
                    xsdType = "xsd:integer"; // was integer
                } else if (type.toLowerCase().equals("date")) {
                    xsdType = "xsd:date"; // was date
                } else {
                    xsdType = "xsd:decimal"; // was decimal
                }

                SPARQLDoer.insertObjectPropQuad(connection, attribute, "rdf:type", "owl:DatatypeProperty");
                SPARQLDoer.insertObjectPropQuad(connection, attribute, "rdfs:domain", tableName);
                SPARQLDoer.insertObjectPropQuad(connection, attribute, "rdf:range", "rdfs:" + xsdType);
            }
        }
    }
    
    //Move up late...
	public String SPARQL = "";
	private boolean DEBUGGING = true;
	SQLValidator validator;
	
    private Collection<String> returns_instances_of = null; 
	/**
	 *
	 */
	public String getSelect(Select select, Collection<String> instance_type_names) throws SQLException, JSQLParserException, ownIllegalSQLException{
		this.returns_instances_of = instance_type_names;
		//Initialize Validator
		validator = new SQLValidator();
		
		site = "www.example.org/"; 
		
		System.out.println("SQL statement: |" + select + "|");	
		//Setting depth for subqueries, asumming subqueries on the where clause
		select.getSelectBody().accept(this);
		
/*		
		System.out.println(select.getWithItemsList() + "\n\n");
		try{
			if(!ownException.equals("")){
				throw new ownIllegalSQLException(ownException);
			}
		} catch (ownIllegalSQLException e){
			System.out.println(e);
			return e.toString();
		}
*/
		SPARQL += subq.pop() + endOfStmt;
		
		//Building SPARQL Statement
		while(!subq.isEmpty()){
    			SPARQL += subq.pop() + endOfStmt + ")";
		}
		
		System.out.println("RDF conversion of select:\n |" + SPARQL + "| END");

		return SPARQL;
	}
	/* Current subquery */
	private String tempSub;
	
	/**
	 *
	 */
	public void visit(PlainSelect plainSelect) {
		//Creting data structures to hold values to build Oracle SQL statement.
		//Done this way since expecting basic forms of subqueries
		List<String> filters = new ArrayList<String>();
		List<String> tables = new ArrayList<String>();
		List<String> matches = new ArrayList<String>();
		List<String> orderby = new ArrayList<String>();
		List<String> subselects = new ArrayList<String>();
		HashMap<String, List<String>> tablesColumns = new HashMap<String, List<String>>();
		
		//FROM thisTable AS a will be store as <a, thisTable>
		HashMap<String,String> tablesAliases = new HashMap<String,String>();

                
                    
		//SELECT thisColumn AS col will be store as <thisColumn, col>
		LinkedHashMap<String,String> columnsAs = new LinkedHashMap<String,String>();
		List<String> internalColumns = new ArrayList<String>();
		
		tempSub = "SELECT " + 
			  visitWithArgs(plainSelect, filters, tables, matches, orderby,
				        tablesColumns, tablesAliases, columnsAs, internalColumns);
 
		
		subq.add(tempSub);
	}
	
	/**
	 *
	 */
	public String visitWithArgs(PlainSelect plainSelect, 
				   List<String> filters, List<String> tables, List<String> matches,
				   List<String> orderby, HashMap<String, List<String>> tablesColumns,  					HashMap<String, String> tablesAliases, LinkedHashMap<String, 
				   String> columnsAs, List<String> internalColumns) { 
		
		String SPARQL = "";
		ownException = "";
		//will contain <nameOfTable, columnsOfTable>
		
		//Adding table from FROM clause
		FromItem fromItem = plainSelect.getFromItem(); //Accepting the visitor
		fromItem.accept(this);
		
		String alias = fromItem.getAlias();
		tables.add(temp);
		
		tablesAliases.put((alias == null ? temp : alias), temp);
		
		//Get column names from current added table, will help on mapping aliases
		try{	
			tablesColumns.put(temp, getAllColsFromTbl(temp));	
		} catch (JSQLParserException e) {	
			System.err.println("Caught JSQLParserException: " + e.getMessage());	
		} catch (SQLException e) {	
			System.err.println("Caught SQLException: " + e.getMessage());	
		}
		
		//plainSelect is the whole statement
		if (plainSelect.getJoins() != null) {
			for (Iterator joinsIt = plainSelect.getJoins().iterator(); joinsIt.hasNext();) {
				Join join = (Join) joinsIt.next();
				fromItem = join.getRightItem();
				fromItem.accept(this);
				//p(temp+"TEMP AT JOINS");	
				//right side of JOIN needs to be stored as a table, proceed as previously 
				alias = fromItem.getAlias();
				tables.add(temp);
				
				try{	
					tablesColumns.put(temp, getAllColsFromTbl(temp));	
				} catch (JSQLParserException e) {	
					System.err.println("Caught JSQLParserException: " + e.getMessage());	
				} catch (SQLException e) {	
					System.err.println("Caught SQLException: " + e.getMessage());	
				}
				//Storing alias
				tablesAliases.put( (alias == null ? temp : alias), temp);
				
				//joins...
				if(join.getOnExpression() != null) {
					join.getOnExpression().accept(this);
					//at this point, the temp just gets converted to the rhs side of the ON expression
					//LEFT SIDE OF JOIN
					String tableName = temp.substring(5, temp.indexOf(" :"));
					// validate left join
					if(!(ownException = validator.validateTable(tablesAliases, tableName, "")).isEmpty()){
						return ownException;
					}
					
					temp = temp.replace(tableName, tablesAliases.get(tableName));
					//RIGHT SIDE OF JOIN
					tableName = temp.substring(temp.lastIndexOf("?this") + 5, temp.lastIndexOf(" :"));
					// validate right join
					if(!(ownException = validator.validateTable(tablesAliases, tableName, "")).isEmpty()){
						return ownException;
					}
					//Add join, fix it first
					temp = temp.replace(tableName, tablesAliases.get(tableName));
					matches.add(temp);	
				}
			}
		}
		if(plainSelect.getSelectItems() != null) {
			//gets the columns that are asked of
			for(Iterator i=plainSelect.getSelectItems().iterator(); i.hasNext();) {
				SelectItem item = (SelectItem)i.next();
				item.accept(this);

				// if selecting everything
				if(temp.equals("*")){
					for(Iterator fI=tables.iterator(); fI.hasNext();) {
						String tbl = (String)fI.next();
                       List<String> cols = null;
                       try {
                          cols = SPARQLDoer.getAllColumns(connection, tbl);
                       } catch (SQLException e) {
	                      System.out.println("Failed to get attributes for class" + e);
                       }
                       for (String col : cols) {
							columnsAs.put(tbl + "." + col, tbl + "." + col);
                       }
					}
				}
				else{
					//Checking for columns aliases
					String tempColumnAs = "";
					
					//If have alias
					Boolean columnAs = false;
					
					if(item.toString().contains(" AS ")){
						String[] split = (item.toString()).split(" AS ");
						tempColumnAs = split[1];
						columnAs = true;
					}
					
					//Get the table name formatted by tablename() method
					String tableName = tablename(temp);
                    if(tableName.equals("tbl")) {
                       List<String> tbl = null;
                       try {
                          tbl = SPARQLDoer.getClassForAttr(connection, temp);
                       } catch (SQLException e) {
	                      System.out.println("Failed to get class for attribute" + e);
                       }
                       if(tbl != null) {
                          if (tbl.size() == 1) for (String s : tbl) tableName = s;
                          else return "Ambiguous column name: " + temp;
                       }
                       temp = tableName + "." + temp;
                    }
                    
					String colName = "";

					//If not table aliases found
					if(tableName.equals("tbl")){
						//validate column

						colName = validator.validateColumn(tablesColumns, colname(temp));
						if(!validator.isValidColumn()){
							return ownException = colName;
						}

					}
					//column with alias
					else{
						//Validate column
						if(!(ownException = validator.validateTable(tablesAliases, tableName, colname(temp))).isEmpty()){
							return ownException;
						}
	
				 		//put the column in proper format
						if(tablesAliases.containsKey(tableName))
							colName = temp.replace(tableName, tablesAliases.get(tableName));
						else{
							colName = temp;
						}
					}
					//store formatted column
					columnsAs.put(colName, (columnAs ? tempColumnAs : colName));
				}
			}
				
		}
		
		//Creating Statement
		SPARQL += getColumns(columnsAs);
		SPARQL += getColumnsVar(columnsAs);
		SPARQL += getJoins(matches);
		/*
		for(String tbl : tables){
			boolean found = false;
			for(String colAs : columnsAs.keySet()){
				if(tablename(colAs).equals(tbl)){
					found = true;
					break;
				}
			}
			int minlen = Math.min(tbl.length(), 6);		
			if(!found){
				SPARQL += "?" + tbl.substring(0, minlen) + " rdf:type :" + tbl + " . \n\t";
			}
		}
		*/
		//Not in a subquery
		sq.push(false);
		join = false;
		matches = new ArrayList<String>();
			
		//System.out.println(SPARQL);
		if (plainSelect.getWhere() != null) { //ie, there's a where clause
			wasEquals = false;
			this.tablesColumns = tablesColumns;
			this.tablesAliases = tablesAliases;
			this.columnsAs = columnsAs;
			this.filters = filters;
			this.matches = matches;
			plainSelect.getWhere().accept(this);
			if(join){
				System.out.println("DDD");
				SPARQL += temp + "\n\t";
				join = false;
			}
			else if(!sq.peek()){
				String tableName = getFilterTableName(temp.trim());
				String dataValue = temp.substring(temp.lastIndexOf(" ") + 1);
				//Right now if is not a numeric value, then it will be consider a string
				if(!isNumeric(dataValue))
					temp =temp.substring(0, temp.lastIndexOf(" ") + 1) + "\"" + temp.substring(temp.lastIndexOf(" ") + 1) + "\"";
				//validate
				if(!tableName.equals("tbl")){
					//validate table name
					if(!(ownException = validator.validateTable(tablesAliases, tableName, temp.substring(temp.indexOf("?")+1,temp.lastIndexOf("_")))).isEmpty()){
						return ownException;
					}
					
					//Add filter
					if(tablesAliases.containsKey(tableName))
						filters.add(temp.replace(tableName, tablesAliases.get(tableName)));
				}
				else{
					String ct = temp.trim().split("\\s+")[0];
					String colName = ct.substring(1, ct.lastIndexOf("_"));
					
					//validate column
					colName = validator.validateColumn(tablesColumns, colName);
					if(!validator.isValidColumn()){
						return ownException = colName;
					}

					//validate table name, it does not contain alias.
					tableName = tablename(colName);
					//validate column
					
					//Add filter
					filters.add(temp.replace("_tbl","_" + tableName));
				}
				sq.pop();
			}
			else {
				sq.pop();
				temp = temp.trim();
				
				//Have Subquery
				String ss = subq.pop();
			
				String tt = temp.substring(0, temp.indexOf(" "));
				
				//Get the table name formatted by tablename() method
				String tableName = tablename(tt);
				
				String colName = "";
				//If not table aliases found
				if((tt.charAt(0) == '?') || !tt.contains("_") && tableName.equals("tbl")){
					//validate column
					
					tableName = getFilterTableName(temp);
					
					String ct = temp.split("\\s+")[0];
					
					int lio_ = ct.lastIndexOf("_");
					colName = (lio_ == -1) ? ct : ct.substring(1, lio_);
					
					colName = validator.validateColumn(tablesColumns, colName);
					
					if(!validator.isValidColumn()){
						return ownException = colName;
					}
					
					tt = colName;
				}
				
				//Creating where clause of subquery
				int io_ = temp.indexOf(" ");
				int lio_ = temp.lastIndexOf(" ");
				if(io_ == lio_)
					lio_ = temp.length();	
						
				String ex = temp.substring(io_, lio_ );
				ss = "\n   WHERE " + colname(tt) + "_" + tt.substring(0, tt.indexOf(".")) + ex + " (" + ss;
				
				subq.push(ss);
				
				//Adding columns of where clause in case is not present...
				for (String item : columnsAs.keySet()) {
					p(item);
				}
				if(!columnsAs.containsKey(tt)){
					LinkedHashMap<String,String> columnsAsTemp = new LinkedHashMap<String, String>();
					columnsAsTemp.put(tt,tt);
					columnsAs.put(tt, tt);
					SPARQL += getColumnsVar(columnsAsTemp);
				}
			}
		}
		
		// Adding Filters		
		SPARQL += getFilters(filters, columnsAs);
		//SPARQL += getFilters(matches, columnsAs);
		SPARQL += getJoins(matches);
		if(plainSelect.getOrderByElements() != null) {
			for(Iterator i=plainSelect.getOrderByElements().iterator(); i.hasNext();) {
				OrderByElement item = (OrderByElement)i.next();
				item.accept(this);
				
				//Right now only handeling DESC and ASC for order by...
				if(!(temp.contains("DESC") || temp.contains("ASC"))){
					//Get table name and validate...
					String tableName = tablename(temp);
					if(!(ownException = validator.validateTable(tablesAliases, tableName, colname(temp))).isEmpty()){
						return ownException;
					}
					
					//Add order by
					if(tablesAliases.containsKey(tableName))
						orderby.add(temp.replace(tableName, tablesAliases.get(tableName)));
					else
						orderby.add(temp);
				}
				else{
					
					//Order by an internal column
					temp = temp.trim();
					String colName = temp.substring(temp.indexOf(" ?") + 2, temp.lastIndexOf("_"));
					String tblName = temp.substring(temp.lastIndexOf("_") + 1, temp.lastIndexOf(" )"));
					String orderBy = tblName + "." + colName;	
					if(tblName.equals("tbl")){
						//Validate column
						colName = validator.validateColumn(tablesColumns, colName);
						if(!validator.isValidColumn()){
							return ownException = colName;
						}
						orderBy = colName;
						temp = temp.replace("tbl", tablename(colName));
					}
				
					//Add internal column
					if(!columnsAs.containsKey(orderBy)){
						internalColumns.add(orderBy);
					}
					//Add order by
					orderby.add(temp);
				}
			}
		}
		//Getting End of Statement, DONE
		endOfStmt = getEndOfStmt(internalColumns, orderby);
		
		//Print everthing store to build SPARQL
		if(DEBUGGING){
			printAllLists(filters, tables, internalColumns, matches, orderby, columnsAs);
		}
		
		return SPARQL;
	}
	private void p(String s){
		System.out.println(s);
	}
	private String endOfStmt = "";	
	/* Stack of boolean to indicate weather or not we are
	 * currently at a subquery
	 */
	private Stack<Boolean> sq = new Stack<Boolean>();
	
	/* Store queries */
	private Stack<String> subq = new Stack<String>();
	
	/**
	 *
	 */
	public void visit(SubSelect subSelect) {	
		sq.push(true);
		subSelect.getSelectBody().accept(this);
		
		temp = temp.substring(0, temp.indexOf("IN") + 2);
	}
	
	/**
	*	
	*/
	private String getColumns(LinkedHashMap<String,String> columnsAs){
        //creating columns for ORACLE SQL from regular SQL statement,
        //current part to create, e.g. SELECT E.A AS CS345, G.B CS370 FROM D AS E, F AS G ...
        //will result in SELECT A_D CS345, B_F CS370 FROM TABLE( SEM_MATCH('SELECT * WHERE {
        //note aliases mapping for table names where done while "visiting"
        
        String s = "";
            
        // If we are returning instances, make sure that we need to return the DBUNIQUEID as part of the select. 
        if (this.returns_instances_of != null)
        {
            for (String return_type : returns_instances_of)
            {
                if (columnsAs.get(return_type+".DBUNIQUEID") == null)
                {
                    columnsAs.put(return_type+".DBUNIQUEID", return_type+".DBUNIQUEID"); 
                }
                
            }

        }
        
        for (String entry : columnsAs.keySet()) {
            System.out.println("SQLVisitor-getColumns, entry: " + entry);
            System.out.println("SQLVisitor-getColumns, colname(entry): " + colname(entry));
            System.out.println("SQLVisitor-getColumns, tablename(entry): " + tablename(entry));
            System.out.println("SQLVisitor-getColumns, columnsAs.get(entry): " + columnsAs.get(entry));
            System.out.println("SQLVisitor-getColumns, colname(columnsAs.get(entry)): " + colname(columnsAs.get(entry)));
            // So if we are returning instances. We need a way to determine which instance the data belongs too.
            // To do this i append the tablename_ as part of the column's "as" statement, so the columns returned will start
            // with the name of the instance for which they belong too. 
            if (returns_instances_of != null)
            {
                s += colname(entry) + "_" + tablename(entry) + " " + tablename(entry)+"_"+colname(columnsAs.get(entry)) + ", ";
            }
            else
            {
                s += colname(entry) + "_" + tablename(entry) + " " + colname(columnsAs.get(entry)) + ", ";
            }
        }
        if(s.length() < 1) return s;
        s = s.substring(0,s.length()-2);
        s += " from table(\n\tSEM_MATCH('SELECT * WHERE {\n\t";
        return s;
	}
	
	/**
	 *
	 */
	public String getColumnsVar(LinkedHashMap<String,String> columnsAs){
			//Following example from above SPARQL query will looks like:
			//	?thisD rdf:type :D .
			//	?thisD :A ?A_D .
			//	?thisF rdf:type :F .
			//	?thisF :B :B_F
			String s = "";
			String currTable = "";
			for (String item : columnsAs.keySet()) {
				String tableName = tablename(item);
				String colName = colname(item);
				String tempTableName = tableName;
				int minlen = Math.min(tempTableName.length(), 6);
				if(!tempTableName.equals(currTable)){
					s += "?" + tempTableName.substring(0, minlen) + " rdf:type :" + tableName + " . \n\t";
					currTable = tempTableName;
				}
				s += "?" + tempTableName.substring(0, minlen) + " :" + colName + " ?" + colName + "_" + tempTableName + " . \n\t";
			}
			return s;
	}
	
	/**
	 *
	 */
	public String getJoins(List<String> matches){
			//Adding JOINS, e.g.FROM D AS E JOIN F AS G ON E.STUDENID = G.STUDENTID
			//will result in:	?thisD :STUDENTID ?j1 . ?thisF :STUDENTID ?j1 .
			String s = "";
			for(Iterator fI=matches.iterator(); fI.hasNext();) {
				String item = (String)fI.next();
				s += item + "\n\t";
			}
			return s;
	}
	
	/**
	 *
	 */
	public String getFilters(List<String> filters, LinkedHashMap<String, String> columnsAs){
			// Adding the filters to SEM_MATCH, e.g WHERE D.GRADES > 80
			//results in: FILTER( GRADES_D > 80)
			String s = "";
			if ( !filters.isEmpty() ) {
				for(Iterator fI=filters.iterator(); fI.hasNext();) {
					String item = ((String)fI.next()).trim();
					//String item = "?PETID_PETS = 1001";
    				String t = item.split("\\s+")[0];
    				String colName = t.substring(0,t.lastIndexOf("_")).replace("?", "");
    				String tblName = t.substring(t.lastIndexOf("_") + 1);
    				int minlen = Math.min(tblName.length(), 6);	
					if(!columnsAs.containsKey(tblName + "." + colName)){
    						s += "?" + tblName.substring(0, minlen) + " :" + colName + " " + t + " .\n\t";
					}
				}

				s += "FILTER ( ";
				for(Iterator fI=filters.iterator(); fI.hasNext();) {
					String item = ((String)fI.next()).trim();
					String tblName = getFilterTableName(item);
					String dataValue = item.substring(item.lastIndexOf(" ") + 1);
					String tempTV = dataValue;
					dataValue = dataValue.replaceAll("^\"|\"$", "");
					
					//Numeric? needed for special format
					if(!isNumeric(dataValue)){
						item = item.replace(tempTV, dataValue);
						item = item.substring(0, item.lastIndexOf(" ") + 1) + "\"" + item.substring(item.lastIndexOf(" ") + 1) + "\"";
					}
					s += item + " ";
					if (fI.hasNext()) {
						s += " && ";
					}
				}
				s += " ) \n\t";
						}
			return s;
	}
	
	/**
	 *
	 */
	public String getEndOfStmt(List<String> internalColumns, List<String> orderby){
			String s = "";
			//INTERNAL COLUMNS
			for (String item : internalColumns) {
				String tblName = tablename(item);
				String colName = colname(item);
    					
				int minlen = Math.min(tblName.length(), 6);
				s += "?" + tblName.substring(0, minlen) + " :" + colName + " ?" + colName + "_" + tblName + " . \n\t";
			}
			//working on progress...
			s += "}";
		
			// Adding order by to SEM_MATCH
			if(orderby.size()>0) {
				s += "\n\t ORDER BY ";
				for(Iterator fI=orderby.iterator(); fI.hasNext();) {
					String item = (String)fI.next();
					s += item + " ";
				}
			}
			//Adding model and alias to ORACLE SQL statement
			s += "',\n\t SEM_MODELS('" + connection.getModel() + "'), ";
			s += "null,\n\t SEM_ALIASES( SEM_ALIAS('', 'http://www.example.org/people.owl#')), null) )";
			return s;
	}
	
	/**
	 *
	 */
	public void printAllLists(List<String> filters, List<String> tables, List<String> internalColumns, 
				  List<String> matches, List<String> orderby, LinkedHashMap<String,String> columnsAs){
		@SuppressWarnings("unchecked")
		List<String> colAs = new ArrayList<String>();
    		for(String s : columnsAs.keySet()){
    			colAs.add(s); 
    		}
		List<List<String>> lists = 
				  Arrays.asList(filters, tables, colAs, internalColumns, matches, orderby);
		String[] listsNames = {"Filters:", "Tables:", "Columns:", "Internal Columns:", 
				       "Matches (or Joins?):", "Order By:"};
		for(int i = 0; i < lists.size(); i++){
			printList(listsNames[i],lists.get(i));
		}
	}
	
	/**
	 *
	 */
	private void printList(String field, List<String> list){ 
		System.out.println(field);
		for(Iterator fI=list.iterator(); fI.hasNext();) {
			System.out.println("\t"+fI.next());
		}
	}
	
	/**
	 *
	 */
	static public String tablename(String item) {
		if(item.indexOf('.')>0)
			return item.substring(0,item.indexOf('.'));
		return "tbl";
	}
	
	/**
	 *
	 */
	static public String getFilterTableName(String str) {
        	return (str.substring(0, str.indexOf(" "))).substring(str.lastIndexOf("_") + 1);
    	}
    
    	/**
	 *
	 */
	static public String colname(String item) {
		if(item.indexOf('.')>0)
			return item.substring(item.indexOf('.')+1);
		return item;
	}
	
	/**
	 *
	 */
	static public String filter(String item, String comparison, String value) {
		String tempTableName = tablename(item);
		return "?" + colname(item) + "_" + tempTableName + " " + comparison + " "  + value;
	}
	/**
	 *
	 */
	public String match(String item, String value, boolean isValue) {
		String joinString = "";
		String tblName = tablename(item);
		if(isValue) {
			join = false;
			// this is the case of columName = someValue
			return " ?" + colname(item) + "_" + tblName + " = " + value;
		} else {
			join = true;
			// this is the case of joining two tables
			// first half of join 
    					
			int minlen = Math.min(tblName.length(), 6);
			joinString += "?" + tblName.substring(0, minlen) + " :" + colname(item) + " ?j" + joinInc + " . ";
			// second half of join
			tblName = tablename(value);
			minlen = Math.min(tblName.length(), 6);
			joinString += "?" + tblName.substring(0, minlen) + " :" + colname(value) + " ?j" + joinInc + " . ";
			joinInc++;

			return joinString;
		}
	}
	
	/**
	 *
	 */
	static public List<String> getAllColsFromTbl(String tableName) throws SQLException, JSQLParserException{
		List<String> tblCols = new ArrayList<String>();
		/* Tmp Comment
		List<String> temp = sd.getAllColumns(connection,tableName);
		for(Iterator fI=temp.iterator(); fI.hasNext();) {
			String col = (String)fI.next();
			if(!col.equals("type"))
				tblCols.add(col);
		}
		*/
		//System.out.println(Arrays.toString(tCols.toArray()));
		return tblCols;
	}

	/**
	 *
	 */
	static public boolean isNumeric(String str){
		DecimalFormatSymbols currentLocaleSymbols = DecimalFormatSymbols.getInstance();
		char minusSign = currentLocaleSymbols.getMinusSign();

		if (!Character.isDigit(str.charAt(0)) && str.charAt(0) != minusSign) 
			return false;

		boolean isDecimal = false;
		char decimalSeparator = currentLocaleSymbols.getDecimalSeparator();

		for (char chr : str.substring(1).toCharArray()){
			if(!Character.isDigit(chr)){
				if(chr == decimalSeparator && !isDecimal){
					isDecimal = true;
					continue;
				}
				return false;
			}
		}
		return true;
	}
	
	/**
	 *
	 */
	public String getKeyByValue(HashMap<String, String> map, String value) {
		for (String entry : map.keySet()) {
			if (map.get(entry).equals(value)) {
				return entry;
			}
		}
		return null;
	}
	
	/**
	 *
	 */
	public void testMethod(String sub) {
		try {
			PlainSelect plainSelect2 = (PlainSelect) ((Select) parserManager.parse(new StringReader(sub))).getSelectBody();        	
			//System.out.println("Plainselect2 : " + plainSelect2);
			visit(plainSelect2);
			}
		catch (JSQLParserException e) {
			System.out.println("Null");
			//return null;
		}
	}

	/**
	 *
	 */
	public void visit(Union union) {
		for (Iterator iter = union.getPlainSelects().iterator(); iter.hasNext();) {
			PlainSelect plainSelect = (PlainSelect) iter.next();
			visit(plainSelect);
		}
	}

	/**
	 *
	 */
	public void visit(Table tableName) {
		temp = tableName.getWholeTableName();
	}
	private boolean sub = false;
	

	public void visit(Addition addition) {
		addition.getLeftExpression().accept(this);
		String t = "(" + temp;
		t += addition.getStringExpression();
		addition.getRightExpression().accept(this);
		t += temp + ") ";
		temp = t;
	}

	public void visit(AndExpression andExpression) {
		wasEquals = false;	//Does the expression has the equals sign?
		andExpression.getLeftExpression().accept(this);
		if(wasEquals) {	//If it does add it to matches
			matches.add(temp);
		} else {
			//Need tablesAliases	
			
			String tableName = getFilterTableName(temp.trim());
			String dataValue = temp.substring(temp.lastIndexOf(" ") + 1);
			//Right now if is not a numeric value, then it will be consider a string
			if(!isNumeric(dataValue))
				temp =temp.substring(0, temp.lastIndexOf(" ") + 1) + "\"" + temp.substring(temp.lastIndexOf(" ") + 1) + "\"";
			//validate
			if(!tableName.equals("tbl")){
				//validate table name
				if(!(ownException = validator.validateTable(tablesAliases, tableName, temp.substring(temp.indexOf("?")+1,temp.lastIndexOf("_")))).isEmpty()){
					return;
				}
				
				//Add filter
				if(tablesAliases.containsKey(tableName))
					filters.add(temp.replace(tableName, tablesAliases.get(tableName)));
			}
			else{
				String ct = temp.trim().split("\\s+")[0];
				String colName = ct.substring(1, ct.lastIndexOf("_"));
					
				//validate column
				colName = validator.validateColumn(tablesColumns, colName);
				if(!validator.isValidColumn()){
					ownException = colName;
					return;
				}

				//validate table name, it does not contain alias.
				tableName = tablename(colName);
				//validate column
					
				//Add filter
				filters.add(temp.replace("_tbl","_" + tableName));
			}

		}
		wasEquals = false;
		andExpression.getRightExpression().accept(this);
	}

	public void visit(Between between) {
		between.getLeftExpression().accept(this);
		between.getBetweenExpressionStart().accept(this);
		between.getBetweenExpressionEnd().accept(this);
	}

	public void visit(Column tableColumn) {
		temp = tableColumn.getWholeColumnName();
	}

	public void visit(Division division) {
		division.getLeftExpression().accept(this);
		String t = "(" + temp;
		t += division.getStringExpression();
		division.getRightExpression().accept(this);
		t += temp + ") ";
		temp = t;
	}

	public void visit(DoubleValue doubleValue) {
		temp = Double.toString(doubleValue.getValue());
	}
	private boolean join;
	public void visit(EqualsTo equalsTo) {
		equalsTo.getLeftExpression().accept(this);
		String item = temp;
		String comparison = "";
	
		equalsTo.getRightExpression().accept(this);
		if(equalsTo.getRightExpression() instanceof Column) {
			join = true; 
			temp = match(item, temp, false);
			wasEquals = true;
		} else {
			temp = match(item, temp, true);
		}
		//wasEquals = true;
	}

	public void visit(Function function) {
	}

	
	public void visit(GreaterThan greaterThan) {
		greaterThan.getLeftExpression().accept(this);
		String item = temp;
		String comparison = "";
		if(greaterThan.isNot()) {
			comparison = "<=";
		} else {
			comparison = ">";
		}
		greaterThan.getRightExpression().accept(this);
		String value = temp;
		temp = filter(item, comparison, value);
	}

	public void visit(GreaterThanEquals greaterThanEquals) {
		greaterThanEquals.getLeftExpression().accept(this);
		String item = temp;
		String comparison = "";
		if(greaterThanEquals.isNot()) {
			comparison = "<";
		} else {
			comparison = ">=";
		}
		greaterThanEquals.getRightExpression().accept(this);
		String value = temp;
		temp = filter(item, comparison, value);
	}

	public void visit(InExpression inExpression) {
		inExpression.getLeftExpression().accept(this);
		String t = temp + " IN ";
		inExpression.getItemsList().accept(this);
		t += temp;
		temp = t;
	}

	public void visit(InverseExpression inverseExpression) {
		inverseExpression.getExpression().accept(this);
	}

	public void visit(IsNullExpression isNullExpression) {
	}

	public void visit(JdbcParameter jdbcParameter) {
	}

	public void visit(LikeExpression likeExpression) {
		visitBinaryExpression(likeExpression);
	}

	public void visit(ExistsExpression existsExpression) {
		existsExpression.getRightExpression().accept(this);
	}

	public void visit(LongValue longValue) {
		temp = longValue.getStringValue();
	}

	public void visit(MinorThan minorThan) {
		minorThan.getLeftExpression().accept(this);
		String item = temp;
		String comparison = "";
		if(minorThan.isNot()) {
			comparison = ">=";
		} else {
			comparison = "<";
		}
		minorThan.getRightExpression().accept(this);
		String value = temp;
		temp = filter(item,comparison, value);
	}

	public void visit(MinorThanEquals minorThanEquals) {
		minorThanEquals.getLeftExpression().accept(this);
		String item = temp;
		String comparison = "";
		if(minorThanEquals.isNot()) {
			comparison = ">";
		} else {
			comparison = "<=";
		}
		minorThanEquals.getRightExpression().accept(this);
		String value = temp;
		temp = filter(item, comparison, value);
	}

	public void visit(Multiplication multiplication) {
		multiplication.getLeftExpression().accept(this);
		String t = "(" + temp;
		t += multiplication.getStringExpression();
		multiplication.getRightExpression().accept(this);
		t += temp + ") ";
		temp = t;
	}

	public void visit(NotEqualsTo notEqualsTo) {
		notEqualsTo.getLeftExpression().accept(this);
		String item = temp;
		String comparison = "!=";
		notEqualsTo.getRightExpression().accept(this);
		String value = temp;
		/*
		if(notEqualsTo.getRightExpression() instanceof Column)
			value += "?"+tablename(temp)+colname(temp);
		else
			value += temp;
		*/
		temp = filter(item, comparison, value);
	}

	public void visit(NullValue nullValue) {
		temp = nullValue.toString();
	}

	public void visit(OrExpression orExpression) {
		orExpression.getLeftExpression().accept(this);
		String left = temp;
		orExpression.getRightExpression().accept(this);
		temp = "FILTER( "+left+" || "+temp+" )";
		wasEquals = false;
	}

	public void visit(Parenthesis parenthesis) {
		String t = "";
		if(parenthesis.isNot())
			t += "!";
		parenthesis.getExpression().accept(this);
		t += "("+temp+")";
		temp = t;
	}

	public void visit(StringValue stringValue) {
		temp = stringValue.getValue();
	}

	public void visit(Subtraction subtraction) {
		subtraction.getLeftExpression().accept(this);
		String t = "(" + temp;
		t += subtraction.getStringExpression();
		subtraction.getRightExpression().accept(this);
		t += temp + ") ";
		temp = t;
	}

	public void visitBinaryExpression(BinaryExpression binaryExpression) {
		binaryExpression.getLeftExpression().accept(this);
		binaryExpression.getRightExpression().accept(this);
	}

	public void visit(ExpressionList expressionList) {
		String t = "(";
		for (Iterator iter = expressionList.getExpressions().iterator(); iter.hasNext();) {
			Expression expression = (Expression) iter.next();
			expression.accept(this);
			t += temp + ", ";
		}
		t = t.substring(0, t.length()-2) + ")";
		temp = t;

	}

	public void visit(DateValue dateValue) {
		temp = dateValue.getValue().toString();
	}
	
	public void visit(TimestampValue timestampValue) {
		temp = timestampValue.getValue().toString();
	}
	
	public void visit(TimeValue timeValue) {
		temp = timeValue.getValue().toString();
	}

	public void visit(CaseExpression caseExpression) {
	}

	public void visit(WhenClause whenClause) {
	}
	
	public void visit(BitwiseXor bitwiseXor) {
	}
	
	public void visit(BitwiseOr bitwiseOr) {
	}
	
	public void visit(BitwiseAnd bitwiseAnd) {
	}
	
	public void visit(Matches matches) {
	}
	
	public void visit(Concat concat) {
	}

	public void visit(AllComparisonExpression allComparisonExpression) {
		allComparisonExpression.GetSubSelect().getSelectBody().accept(this);
	}

	public void visit(AnyComparisonExpression anyComparisonExpression) {
		anyComparisonExpression.GetSubSelect().getSelectBody().accept(this);
	}

	public void visit(SubJoin subjoin) {
		subjoin.getLeft().accept(this);
		subjoin.getJoin().getRightItem().accept(this);
	}
	
	//SELECT ItemS
	public void visit(AllColumns columns) {
		temp = "*";
	}
	
	public void visit(AllTableColumns columns) {
		temp = "*";
	}
	
	public void visit(SelectExpressionItem item) {
		item.getExpression().accept(this);
	}
	
	//Order by visitor
	public void visit(OrderByElement order) {
		order.getExpression().accept(this);
		if(order.isAsc()) {
			temp = " ASC( ?" + colname(temp) + "_" + tablename(temp) + " )";
		} else {
			temp = " DESC( ?" + colname(temp) + "_" + tablename(temp) + " )";
		}
		/*
		if(!order.isAsc()) {
			temp = "DESC(?"+tablename(temp)+colname(temp)+")";
		} else {
			temp = "ASC(?"+tablename(temp)+colname(temp)+")";
		}
		*/
	}
	private class ownIllegalSQLException extends IllegalArgumentException
	{
	public ownIllegalSQLException( String message )
	{
	  super( message );
	}
	}

}
