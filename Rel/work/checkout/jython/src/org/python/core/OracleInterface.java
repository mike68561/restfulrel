package org.python.core;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * A class that is used to interface with the oracle database.
 * All statements and queries should go through here to communicate with oracle.
 */
 
public class OracleInterface extends DatabaseInterface {

    private Connection connection;
    private Statement statement; 
    private ResultSet rs;
     
    public OracleInterface(String url, String uname, String passw, String conn_type) {
        super();
        try
        {
            Class.forName("oracle.jdbc.OracleDriver");
        }
            catch(ClassNotFoundException e)
        {
            e.printStackTrace();
        }
        connection = null;
        statement = null;
        if(conn_type != "none" ) {
        try
           {
              // Connect to the database
              connection = DriverManager.getConnection(url, uname, passw);
           } catch(SQLException e)
           {
                 System.out.println("Exception: " + e.getMessage());
           }
        }
    }

    @Override
    public void executeStatement(String stmt)
    {
        try
        {
            System.out.println("exec -> " + stmt);
            statement = connection.createStatement();
            statement.execute(stmt); 
        }   catch(java.sql.SQLException e)
        {
               System.out.println("Exception: " + e.getMessage());
        }
    }

    @Override
    public ResultSet executeQuery(String query)
    {
        try
        {
            statement = connection.createStatement();
            rs = statement.executeQuery(query); 
        }   catch(java.sql.SQLException e)
        {
               System.out.println("exception: " + e.getMessage());
        }
        return rs;
    }
}
